package App::Docker::CLI::Image;

use 5.18.0;
use strict;
use warnings;

use parent qw/App::Docker::CLI/;

use Tree::Simple;
use Tree::Simple::View::ASCII;
use App::Docker::Client::Image;
our $VERSION = '0.010000';

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    $self->{_client} = App::Docker::Client::Image->new( %{ $self->{client} } );
    bless $self, $class;
    return $self;
}

=head2 build

=cut

sub build {
    my $self  = shift;
    my $image = $self->{_client}->build(
        {
            t => $self->{tag},
            ( $self->{remote}      ? ( remote      => $self->{remote} )      : () ),
            ( $self->{quiet}       ? ( q           => $self->{quiet} )       : () ),
            ( $self->{no_cache}    ? ( nocache     => $self->{no_cache} )    : () ),
            ( $self->{cachefrom}   ? ( cachefrom   => $self->{cachefrom} )   : () ),
            ( $self->{pull}        ? ( pull        => $self->{pull} )        : () ),
            ( $self->{rm}          ? ( rm          => $self->{rm} )          : () ),
            ( $self->{forcerm}     ? ( forcerm     => $self->{forcerm} )     : () ),
            ( $self->{memory}      ? ( memory      => $self->{memory} )      : () ),
            ( $self->{memswap}     ? ( memswap     => $self->{memswap} )     : () ),
            ( $self->{cpushares}   ? ( cpushares   => $self->{cpushares} )   : () ),
            ( $self->{cpusetcpus}  ? ( cpusetcpus  => $self->{cpusetcpus} )  : () ),
            ( $self->{cpuperiod}   ? ( cpuperiod   => $self->{cpuperiod} )   : () ),
            ( $self->{cpuquota}    ? ( cpuquota    => $self->{cpuquota} )    : () ),
            ( $self->{buildargs}   ? ( buildargs   => $self->{buildargs} )   : () ),
            ( $self->{shmsize}     ? ( shmsize     => $self->{shmsize} )     : () ),
            ( $self->{squash}      ? ( squash      => $self->{squash} )      : () ),
            ( $self->{labels}      ? ( labels      => $self->{labels} )      : () ),
            ( $self->{networkmode} ? ( networkmode => $self->{networkmode} ) : () ),
        },
        { ( $self->{file} ? ( dockerfile => $self->{file} ) : () ) },
        { content_type => 'application/tar' },
        $self->_callback,
    );
    print $image->latest() . ': ' . $image->id(), $/;
}

=head2 inspect

=cut

sub inspect {
    my $self  = shift;
    my $image = $self->{_client}->inspect( $self->{image} );
}

=head2 list

=cut

sub list {
    my $self = shift;
    my $options = { ( defined $self->{all} ? ( all => "true" ) : () ), };
    _draw_list( $self->{_client}->list($options) );
}

=head2 remove

=cut

sub remove {
    my ($self) = @_;
    $self->{image} = $self->{argv}->[-1] if !$self->{dangling} && !defined $self->{image};
    my $list = $self->{image} ? [ $self->{image} ] : [];
    my ( $tag, $version ) = split ':', $self->{image} if defined $self->{image};

    my $client = $self->{_client};
    $list = $client->list_all_versions($tag) if ( defined $version && $version eq '*' );
    $list = $client->list( { filter => '{"dangling":true}' }, { log => 'trace' } ) if ( $self->{dangling} );
    for my $name (@$list) {
        my $image = $client->inspect($name);
        $image = $self->{_client}->remove($name);
        print $image->latest() . ': ' . $image->id(), $/;
        for my $untagged ( @{ $image->untagged } ) {
            print $_ . ': ' . $untagged->{$_}, $/ for ( keys %$untagged );
        }
    }
}

=head2 pull

=cut

sub pull {
    my $self = shift;
    $self->{image} ||= shift @{ $self->{argv} };
    require Term::ANSITable;
    my $table = Term::ANSITable->new( columns => [ "Id", "Progress", "Status" ] );
    $table->table->set_column_style( 'Progress', width => 80 );
    require Class::Docker::Image;
    my $image =
      $self->{_client}->pull( Class::Docker::Image->new( RepoTags => [ $self->{image} ] ), $self->_callback($table) );
    $table->refresh_table->draw;
}

=head2 treeview

=cut

sub treeview {
    my ($self) = @_;
    my $tree_view = Tree::Simple::View::ASCII->new(
        Tree::Simple->new('images')->addChildren( _images_to_tree( $self->{_client}->tree ) ),
        (
            characters => [
                indent      => "    ",
                pipe        => " │  ",
                last_branch => " └─ ",
                branch      => " ├─ "
            ]
        )
    );
    $tree_view->includeTrunk(1);
    print $tree_view->expandAll();
}

{

=head2 _images_to_tree

=cut

    sub _images_to_tree {
        my $images = shift;
        my @trees  = ();
        for (@$images) {
            shift @{ $_->tags } if ( $_->tags && $_->tags->[0] eq "<none>:<none>" );
            my $tree = Tree::Simple->new(
                (
                    $_->tags && scalar @{ $_->tags } > 0
                    ? join ', ', @{ $_->tags }
                    : $_->id =~ s/sha256://r
                )
            );
            $tree->addChildren( _images_to_tree( $_->children ) ) if ( scalar @{ $_->children } > 0 );
            push @trees, $tree;
        }
        return @trees;
    }

=head2 _callback

Simple error handler returns undef if everything is ok dies on error.

=cut

    sub _callback {
        my ( $self, $table ) = @_;
        my $response_cache;
        my $progress = {};
        return sub {
            my ( $chunk, $res, $proto ) = @_;
            $response_cache .= $chunk;
            my $res_chunk = $self->{_client}->to_hashref($response_cache);

            if ( ref $res_chunk eq 'HASH' ) {
                print $res_chunk->{errorDetail}->{message}, $/ if ( $res_chunk->{errorDetail} );
                if ( $res_chunk->{status} ) {
                    _update_table( $table, $progress, $res_chunk );
                    $table->refresh_table->draw(1);
                }
                print $res_chunk->{stream} if ( $res_chunk->{stream} );
                $response_cache = '';
            }
        };
    }

    sub _update_table {
        my ( $table, $progress, $res ) = @_;
        return if !$res->{id};

        if ( !defined $progress->{ $res->{id} } ) {
            $progress->{ $res->{id} } = [ $res->{id}, $res->{progress}, $res->{status} ];
            $table->add_row( $progress->{ $res->{id} } );
            return;
        }

        $progress->{ $res->{id} }->[1] = $res->{progress};
        $progress->{ $res->{id} }->[2] = $res->{status};
    }

    sub _draw_list {
        my ($list) = @_;
        require Term::ANSITable;
        my $table = Term::ANSITable->new( columns => [ 'Id', 'Tags' ] );
        $table->table->color_theme('Default::no_color');
        foreach (@$list) {
            $table->add_row( [ $_->id, join ', ', @{ $_->tags || [] } ] );
        }
        $table->draw;
    }
}

1;    # End of App::Docker::CLI::Image

__END__

=pod

=head1 NAME

App::Docker::CLI::Image - The great new App::Docker::CLI::Image!

=head1 VERSION

Version 0.010000

=head1 SUBROUTINES/METHODS

=cut

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::Docker::CLI::Image
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut
