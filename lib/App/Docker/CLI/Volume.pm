package App::Docker::CLI::Volume;

use 5.18.0;
use strict;
use warnings;

use parent qw/App::Docker::CLI/;

use App::Docker::Client::Volume;

=head1 NAME

App::Docker::CLI::Volume - The great new App::Docker::CLI::Volume!

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head1 SUBROUTINES/METHODS

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    $self->{client} = App::Docker::Client::Volume->new( %{ $self->{client} } );
    bless $self, $class;
    return $self;
}

=head2 inspect

run on command line.

=cut

sub inspect {
    my $self = shift;
    $self->{name} = $self->{argv}->[-1] unless $self->{name};
    require App::Docker::Utils::Volume;
    my $volume = $self->{client}->inspect( App::Docker::Utils::Volume::cmd2volume_object($self) );
    _draw_volumes( [$volume] );
    _draw_labels( $volume->labels )   if ( scalar keys %{ $volume->labels } > 0 );
    _draw_options( $volume->options ) if ( scalar keys %{ $volume->options } > 0 );

}

=head2 list

run on command line.

=cut

sub list {
    my $self = shift;
    my $result = $self->{client}->list( { ( $self->{dangling} ? ( dangling => ['true'] ) : () ) } );

    if ( $self->{quiet} ) {
        print $_->name, $/ for @{ $result->{Volumes} };
        return;
    }
    _draw_volumes( $result->{Volumes} );
}

=head2 create

run on command line.

=cut

sub create {
    my ($self) = @_;
    require App::Docker::Utils::Volume;
    my $volume = $self->{client}->create( App::Docker::Utils::Volume::cmd2volume_object($self) );
    return print $volume->name if ( $self->{quiet} );
    _draw_volumes( [$volume] );
    _draw_labels( $volume->labels )   if ( scalar keys %{ $volume->labels } > 0 );
    _draw_options( $volume->options ) if ( scalar keys %{ $volume->options } > 0 );
}

=head2 remove

=cut

sub remove {
    my $self = shift;
    $self->{name} = $self->{argv}->[-1] unless $self->{name};
    require App::Docker::Utils::Volume;
    my $volume = $self->{client}->remove( App::Docker::Utils::Volume::cmd2volume_object($self) );
}

=head2 prune

=cut

sub prune {
    my $self = shift;
}

=head2 _draw_labels

=cut

sub _draw_labels {
    my $labels = shift;
    require Text::ANSITable;
    my $table = Text::ANSITable->new( columns => [ 'Labels', '' ] );
    $table->add_row( [ $_, $labels->{$_} ] ) for keys %$labels;
    print $table->draw;
}

=head2 _draw_labels

=cut

sub _draw_options {
    my $options = shift;
    require Text::ANSITable;
    my $table = Text::ANSITable->new( columns => [ 'Options', '' ] );
    $table->add_row( [ $_, $options->{$_} ] ) for keys %$options;
    print $table->draw;
}

=head2 _draw_labels

=cut

sub _draw_volumes {
    my $volumes = shift;
    require Text::ANSITable;
    my $table = Text::ANSITable->new( columns => [ 'Driver', 'Name', 'Scope', 'Mountpoint', 'Labels', 'Options' ] );
    $table->set_column_style( 'Name',       width => 65 );
    $table->set_column_style( 'Mountpoint', width => 95 );
    $table->add_row(
        [
            $_->driver, $_->name, $_->scope, $_->mountpoint,
            scalar keys %{ $_->labels  || {} },
            scalar keys %{ $_->options || {} }
        ]
    ) for @{$volumes};
    print $table->draw;
}

1;    # End of App::Docker::CLI::Volume

__END__

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::Docker::CLI::Volume
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut
