package App::Docker::CLI::Converter::Container;

use 5.18.0;
use strict;
use warnings;
use Ref::Util qw/is_arrayref/;

=head1 NAME

App::Docker::CLI::Converter

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head1 SYNOPSIS


=head1 EXPORT

=head1 SUBROUTINES/METHODS

=head2 cmd2container_object

=cut

sub cmd2container_object {
    my ($cmd) = @_;
    require Class::Docker::Container;
    require Class::Docker::Network::Container;
    my $network = Class::Docker::Network::Container->new( ( IPAddress => $cmd->{ip} ) )
      if $cmd->{ip};
    my $network_settings = { Networks => { $cmd->{net} => $network } }
      if ( $network && $cmd->{net} );
    my $portbindings = parse_portbindings( $cmd->{publish} );
    my $host_config  = {
        ( $cmd->{restart} ? ( RestartPolicy => { Name => $cmd->{restart} } ) : () ),
        ( $portbindings ? ( PortBindings => $portbindings ) : () ),
    };

    my $config = {
        Cmd => $cmd->{cmd} || 'sh',
        ( $cmd->{tty}         ? ( Tty       => 'true' ) : () ),
        ( $cmd->{interactive} ? ( OpenStdin => 'true' ) : () ),
    };
    my $mounts = parse_volumes( $cmd->{volume} ) if ( $cmd->{volume} );
    require Acme::NameGen::CPAN::Authors;
    return Class::Docker::Container->new(
        (
            Name => $cmd->{name} ? $cmd->{name} : Acme::NameGen::CPAN::Authors::gen_lc(),
            Image => $cmd->{image},
            ( $cmd->{detach} ? ( Detach => 'true' ) : () ),
            ( $cmd->{volumes_from} ? ( VolumesFrom => [ split ',', $cmd->{volumes_from} ] ) : () ),
            ( $network_settings ? ( NetworkSettings => $network_settings ) : () ),
            ( $mounts           ? ( Mounts          => $mounts )           : () ),
            ( $host_config && scalar keys %$host_config > 0 ? ( HostConfig => $host_config ) : () ),
            ( $config      && scalar keys %$config > 0      ? ( Config     => $config )      : () ),
        )
    );

}

=head2 parse_volumes

=cut

sub parse_volumes {
    my ($volume_string) = @_;
    require Class::Docker::Container::Mount;
    my @mounts = ();
    for ( split ',', $volume_string ) {
        my ( $source, $destination, $state ) = split ':', $_;
        push @mounts,
          Class::Docker::Container::Mount->new(
            (
                Source      => $source,
                Destination => $destination,
                ( defined $state && $state eq 'ro' ? ( Mode => 'ro', RW => 0 ) : () ),
            )
          );
    }
    return \@mounts;
}

=head2 parse_portbindings

=cut

sub parse_portbindings {
    my ($bindings) = @_;
    return unless $bindings;
    my %binds = ();    # { "8080/tcp": [{ "HostPort": "11022" }]}
    for ( split ',', $bindings ) {
        my ( $host, $container ) = split ':', $_;
        $binds{ $container . '/tcp' } = [
            {
                HostPort => $host
            }
        ];

    }
    return scalar keys %binds > 0 ? \%binds : ();
}

=head2 container2commandline

=cut

sub container2commandline {
    my ($container) = @_;
    return undef if !$container->isa('Class::Docker::Container');
    my $cmd         = [ 'docker', 'run' ];
    my $config      = $container->config;
    my $host_config = $container->host_config;
    push @$cmd,
      (
          $config
        ? $config->open_stdin && $config->tty
              ? '-it'
              : $config && $config->tty ? '-t'
            : $config && $config->open_stdin ? '-i'
            : ()
        : ()
      );
    push @$cmd, $container->host_config->auto_remove    ? '--rm'             : ();
    push @$cmd, $container->privileged                  ? '--privileged'     : ();
    push @$cmd, $host_config->restart_policy->is_always ? '--restart=always' : ()
      if ( $host_config && $host_config->restart_policy );
    push @$cmd, ( '--name', $container->name =~ s~^/~~r ) if ( $container->name );
    push @$cmd, ( '-v', join ':', ( $_->source, $_->destination, $_->rw ? () : 'ro' ) ) for @{ $container->mounts };
    my $network_settings = $container->network_settings;
    my $networks         = $network_settings->networks;

    for ( ( scalar keys %$networks ? keys %$networks : [] ) ) {
        my $network = $networks->{$_};
        push @$cmd, ( '--ip', $network->ip_address ) if $network->ip_address;
        push @$cmd, ( '--net', $_ );
    }

    if ( $host_config->port_bindings ) {
        push @$cmd, ( '-p', join ':', ( $_->host_port, $_->private_port ) )
          for map { is_arrayref $_ ? @$_ : $_ } values %{ $host_config->port_bindings };
    }

    push( @$cmd, ( '-e', $_ ) ) for @{ ( $config ? $config->env || [] : [] ) };

    my $image = $container->image;
    push @$cmd, $image->latest;
    if ( $container->args && scalar @{ $container->args } ) {
        push @$cmd, $container->path;
        push @$cmd, map { ( $_ =~ / / ) ? qq~"$_"~ : $_ } @{ $container->args };
    }
    return $cmd;
}

1;    # End of App::Docker::CLI::Converter

__END__

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::Docker::CLI::Converter
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut
