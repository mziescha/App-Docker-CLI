package App::Docker::CLI::Container;

use 5.18.0;
use strict;
use warnings;

use parent qw/App::Docker::CLI/;

use App::Docker::Client::Container;

=head1 NAME

App::Docker::CLI::Container - Command line tool for App::Docker::Client::Container.

=head1 VERSION

Version 0.010000

=cut

our $VERSION = '0.010000';

=head1 SUBROUTINES/METHODS

=head2 new

Constructor

=cut

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    $self->{_client} = App::Docker::Client::Container->new( %{ $self->{client} } );
    bless $self, $class;
    return $self;
}

=head2 create

=cut

sub create {
    my ( $self, $container ) = @_;
    unless ($container) {
        require App::Docker::CLI::Converter::Container;
        $self->{image} ||= shift @{ $self->{argv} };
        $container = App::Docker::CLI::Converter::Container::cmd2container_object($self);
    }
    my $image_client = App::Docker::Client::Image->new( %{ $self->{client} } );
    if ( !$self->{quiet} ) {
        require Term::ANSITable;
        my $table = Term::ANSITable->new( columns => [ "Id", "Progress", "Status" ] );
        $table->table->set_column_style( 'Progress', width => 80 );
        require App::Docker::Client::Image;
        $image_client->pull( $container->image, $self->_image_callback($table) );
        $table->refresh_table->draw;
    }
    else {
        $image_client->pull( $container->image );
    }
    my $created_container = $self->{_client}->create($container);
    _draw_containers( [$created_container], $self->{quiet} );
    return $created_container;
}

=head2 exec

=cut

sub exec {
    my ($self) = @_;
    require App::Docker::CLI::Converter::Container;
    my $obj_container =
      App::Docker::CLI::Converter::Container::cmd2container_object( { name => shift @{ $self->{argv} } } );
    $obj_container->id( $obj_container->name ) unless $obj_container->id;
    $self->{_client}->exec(
        $obj_container,
        {
            AttachStdin  => 0,
            AttachStdout => 1,
            AttachStderr => 1,
            DetachKeys   => "ctrl-p,ctrl-q",
            Tty          => 0,
            Cmd          => $self->{argv},
            #Env          => [ "FOO=bar", "BAZ=quux" ],
        },
        { std_in => \*STDIN, std_out => \*STDOUT, }
    );
}

=head2 inspect

=cut

sub inspect {
    my ($self) = @_;
    my $container_id = $self->{container} || $self->{argv}->[-1];
    my $container = $self->{_client}->inspect($container_id);

    #TODO draw inspected container
}

=head2 start

=cut

sub start {
    my ( $self, $container ) = @_;
    $container = $self->{_client}->inspect( $self->{container} || shift @{ $self->{argv} } ) unless $container;
    *STDOUT->autoflush(1);
    my $cv = $self->{_client}->start(
        $container,
        {
            stream => 1,
            stdin  => 1,
            stderr => 1,
            stdout => 1,
        },
        { std_in => \*STDIN, std_out => \*STDOUT, }
    );
    return unless $cv;
    $cv->recv;
}

=head2 stop

=cut

sub stop {
    my ($self) = @_;
    my $options;
    if ( $self->{time} ) {
        $options = { time => $self->{time} };
        delete $self->{time};
    }
    $self->{name} = $self->{argv}->[-1] unless $self->{name};

    require App::Docker::CLI::Converter::Container;
    my $container = App::Docker::CLI::Converter::Container::cmd2container_object($self);
    $container->id( $container->name ) unless $container->id;
    return $self->{_client}->stop( $container, { time => $self->{time} } );
}

=head2 list

=cut

sub list {
    my $self  = shift;
    my $query = {
        ( $self->{all} ? ( all => "true" ) : () ),
        (
            $self->{image}
            ? ( filters => $self->{_client}->json->encode( { ancestor => [ $self->{image} ] } ) )
            : ()
        ),
    };
    if ( $self->{quiet} ) {
        print $_->id, $/ for @{ $self->{_client}->list($query) };
        return;
    }
    _draw_containers( $self->{_client}->list($query) );
}

=head2 logs

=cut

sub logs {
    my ($self) = @_;
    $self->{name} = $self->{argv}->[-1] unless $self->{container};
    require App::Docker::CLI::Converter::Container;
    my $container = $self->inspect;
    $container = App::Docker::CLI::Converter::Container::cmd2container_object($self);
    $container->id( $container->name ) unless $container->id;
    *STDOUT->autoflush(1);
    $self->{_client}->logs(
        $container,
        {
            stdout     => $self->{stdout}     // 1,
            stderr     => $self->{stderr}     // 1,
            timestamps => $self->{timestamps} // 0,
            tail       => $self->{tail}       // 200,
            follow     => $self->{follow}     // 0
        },
        { std_in => \*STDIN, std_out => \*STDOUT, },
    );
}

=head2 remove

=cut

sub remove {
    my ( $self, $container ) = @_;
    shift @{ $self->{argv} } if $self->{force};
    $self->{name} = $self->{argv} unless $self->{name};
    require App::Docker::CLI::Converter::Container;
    for ( @{ $self->{argv} } ) {
        $self->{name} = $_;
        $container = App::Docker::CLI::Converter::Container::cmd2container_object($self);
        $container->id( $container->name ) unless $container->id;
        $self->{_client}->with_valid_code(404)
          ->remove( $container, { ( ( $self->{force} ? ( force => 'true' ) : () ) ) } );
    }
}

=head2 c2c
=cut

sub c2c {
    my $self = shift;
    require App::Docker::CLI::Converter::Container;
    if ( $self->{container} ) {
        my $container = $self->{_client}->inspect( $self->{container} );
        $container->image(
            App::Docker::Client::Image->new( %{ $self->{client} } )->inspect( $container->image->latest ) );
        print join ' ', @{ App::Docker::CLI::Converter::Container::container2commandline($container) }, $/;
        return 1;
    }
    print join ' ', @{ App::Docker::CLI::Converter::Container::container2commandline($_) }, $/
      for @{ $self->{_client}->list };
}

=head2 run

create and run container

=cut

sub run {
    my ($self) = @_;
    require App::Docker::CLI::Converter::Container;
    if ( $self->{rm} ) {
        $self->{_client}->with_valid_code(404)->remove(
            App::Docker::CLI::Converter::Container::cmd2container_object($self),
            { ( ( $self->{force} ? ( force => 'true' ) : () ) ) }
        );
    }
    $self->{image} ||= shift @{ $self->{argv} };
    $self->{cmd}   ||= shift @{ $self->{argv} };
    my $container = $self->create( App::Docker::CLI::Converter::Container::cmd2container_object($self) );
    return if !defined $container || !defined $container->id;
    $self->start($container);
}

=head1 PRIVATE SUBROUTINES/METHODS

=cut

{

=head2 _image_callback

=cut

    sub _image_callback {
        my ( $self, $table ) = @_;
        my $response_cache;
        my $progress = {};
        return sub {
            my ( $chunk, $res, $proto ) = @_;
            $response_cache .= $chunk;
            my $res_chunk = $self->{_client}->to_hashref($response_cache);

            if ( ref $res_chunk eq 'HASH' ) {
                print $res_chunk->{errorDetail}->{message}, $/ if ( $res_chunk->{errorDetail} );
                if ( $res_chunk->{status} ) {
                    _update_table( $table, $progress, $res_chunk );
                    $table->refresh_table->draw(1);
                }
                print $res_chunk->{stream} if ( $res_chunk->{stream} );
                $response_cache = '';
            }
        };
    }

=head2 _draw_labels

=cut

    sub _draw_options {
        my $options = shift;
        require Text::ANSITable;
        my $table = Text::ANSITable->new( columns => [ 'Options', '' ] );
        $table->add_row( [ $_, $options->{$_} ] ) for keys %$options;
        print $table->draw;
    }

=head2 _draw_containers

=cut

    sub _draw_containers {
        my $containers = shift;
        my $quiet      = shift;
        require Text::ANSITable;
        my $table = Text::ANSITable->new( columns => [ 'Id', 'Image', 'Name', 'Created', 'Ports' ] );
        $table->set_column_style( 'Id', width => 65 );
        my $name_lenght  = 0;
        my $ports_lenght = 0;
        for ( @{$containers} ) {

            if ($quiet) {
                print $_->id, $/;
                next;
            }
            my $config        = $_->config;
            my $network_ports = $_->network_settings->ports;
            my $image         = $_->image;
            my $ports;
            for ( keys %$network_ports ) {
                my $bind = $network_ports->{$_};
                $ports .= $bind ? $bind->[0]->{HostIp} . ':' . $bind->[0]->{HostPort} . ' => ' . $_ : $_;
            }
            $table->add_row( [ $_->id, $image->latest, $_->name =~ s/^\///r, $_->created, $ports ] );
            $name_lenght = length( $_->name ) if length( $_->name ) > $name_lenght;
            $ports_lenght = length($ports) if $ports && length($ports) > $ports_lenght;
        }
        return if ($quiet);

        $table->set_column_style( 'Name', width => $name_lenght );
        print $table->draw;
    }
}

1;    # End of App::Docker::CLI::Container

__END__

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-docker at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=App-Docker>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::Docker::CLI::Container
You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=App-Docker>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/App-Docker>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/App-Docker>

=item * Search CPAN

L<http://search.cpan.org/dist/App-Docker/>

=back

=head1 LICENSE AND COPYRIGHT


Copyright 2017 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=cut
