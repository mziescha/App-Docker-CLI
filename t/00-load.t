#!perl -T
use 5.18.0;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'App::Docker::CLI' ) || print "Bail out!\n";
}

diag( "Testing App::Docker::CLI $App::Docker::CLI::VERSION, Perl $], $^X" );
